<?php

Route::get('/', 'TasksController@index');
Route::get('/tasks/{task}', 'TasksController@show');
Route::get('/task/create', 'TasksController@create');

Route::post('/tasks', 'TasksController@store');
Route::post('/tasks/{task}/subtasks', 'SubtasksController@store');

Route::get('/about', function () {
    return view('about');
});

Route::auth();
