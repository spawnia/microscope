<?php

namespace App\Http\Controllers;

use App\Task;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $tasks = Task::incomplete()->latest()->get();
        
        return view('tasks.list', compact('tasks'));
    }
    
    public function show(Task $task)
    {
        return view('tasks.show', compact('task'));
    }
    
    public function create()
    {
        return view('tasks.create');
    }
    
    public function store()
    {
        $this->validate(request(), [
            'title'       => 'required',
            'description' => 'required',
        ]);
        
        Task::create([
            'user_id'     => auth()->id(),
            'title'       => request('title'),
            'description' => request('description'),
        ]);
        
        return redirect('/');
    }
}
