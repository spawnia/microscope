<?php

namespace App\Http\Controllers;

use App\Task;
use App\Subtask;

class SubtasksController extends Controller
{
    public function store(Task $task)
    {
        $this->validate(request(), [
            'title' => 'required',
        ]);
        
        $task->addSubtask(
            new Subtask(request(['title']))
        );
        
        return back();
    }
}
