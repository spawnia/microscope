<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 *
 * @mixin \Eloquent
 * @package App
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task incomplete()
 */
class Task extends Model
{
    protected $fillable = ['title', 'description', 'user_id'];
    
    /**
     * @param $query Builder
     * @return mixed
     */
    public function scopeIncomplete($query)
    {
        return $query->where('completed', 0);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function subtasks()
    {
        return $this->hasMany(Subtask::class);
    }
    
    public function addSubtask($title)
    {
        $this->subtasks()->save($title);
    }
}
