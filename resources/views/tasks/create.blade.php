@extends('layouts.master')

@section('content')
    <form method="POST" action="{{ url('tasks') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Do a thing" required>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description"
                      placeholder="I am going to do it like this..." required></textarea>
        </div>

        <div class="form-group">
        <button type="submit" class="btn btn-primary">Create Task</button>
        </div>
        @include('components.errors')
    </form>
@endsection
