@extends('layouts.master')

@section('content')
    <div class="jumbotron">
        <h1>{{ $task->title }}</h1>
        <p>{{ $task->created_at->toFormattedDateString() }}</p>
        <p>{{ $task->description }}</p>

        <hr>

        <div class="comments">
            <ul class="list-group">
                @foreach($task->subtasks as $subtask)
                    <li class="list-group-item">
                        {{ $subtask->title }}
                    </li>
                @endforeach
            </ul>
            <form method="POST" action="/tasks/{{ $task->id }}/subtasks">
                {{ csrf_field() }}
                <div class="input-group">
                    <input class="form-control" type="text" id="title" name="title" placeholder="New subtask" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">Add Subtask</button>
                    </span>
                </div>
                @include('components.errors')
            </form>
        </div>
    </div>
@endsection
