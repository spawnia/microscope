@extends('layouts.master')

@section('content')
    <ul class="list-group">
        @foreach($tasks as $task)
            <li class="list-group-item">
                <a href="/tasks/{{$task->id}}"><strong>{{ $task->title }}</strong></a>
                <span>{{ $task->created_at->toFormattedDateString() }}</span>
            </li>
        @endforeach
    </ul>
@endsection
